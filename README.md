**If you want you can create new EKS:**
-     cd $HOME/elshan/terraform
-     terraform init
-     terraform plan -var-file infrastructure.tfvars
-     terraform apply -var-file infrastructure.tfvars

**If you want to destroy EKS:**
`terraform destroy -var-file infrastructure.tfvars`

-------------------------------------------------------------------------------------------------------------------------------
**For getting rights to work with kubectl:**
`aws eks --region $(terraform output -raw region) update-kubeconfig --name $(terraform output -raw cluster_name)`


**For accesing kubernetes dashboard**


_Getting bearer token_
`kubectl -n kubernetes-dashboard get secret $(kubectl -n kubernetes-dashboard get sa/dashboard-admin -o jsonpath="{.secrets[0].name}") -o go-template="{{.data.token | base64decode}}"`


_Creating tunnel with following command:_
`kubectl proxy`


_Access WEB DASBHOARD UI by clicking this url:_
`http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/`


_After finishing, remove SA and ClusterRoleBinding for security concerns._
```
kubectl -n kubernetes-dashboard delete serviceaccount admin-user
kubectl -n kubernetes-dashboard delete clusterrolebinding admin-user
```


-------------------------------------------------------------------------------------------------------------------------------
**For installing HELM:**
1.     chmod u+x get_helm.sh
2.     ./get_helm.sh



**For deploying ingress controller:**
1.     chmod u+x ingress-helm.sh
2.     ./ingress-helm.sh


**For deploying kubernetes manifests:**
   You can use `kubectl apply -f 'manifest_name'` command on kubernetes_files folder. 

   
**You can check the ingress:**
```
curl -i -H "Host: apache.example.com" (your_aws_lb_endpoint).us-east-1.elb.amazonaws.com/
OR
curl -i -H "Host: nginx.example.com" (your_aws_lb_endpoint).us-east-1.elb.amazonaws.com/
```
