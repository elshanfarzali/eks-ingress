#!/bin/bash
echo "Starting script..."
echo `kubectl get deployments | grep plutoview` &> /dev/null
if [ "$?" -eq 0 ]; then
    echo "Ingress already was deployed"
else
    if [ -d $HOME/elshan/ingress-nginx ]; then
        cd ../../ingress-nginx
        helm install eks-ingress -n default -f values.yaml .
    else
        helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
        helm repo update
        helm fetch ingress-nginx/ingress-nginx
        tar -xvf ingress-nginx-4.0.18.tgz
        cd ../../ingress-nginx
        helm install eks-ingress -n default -f values.yaml .
    fi
fi
